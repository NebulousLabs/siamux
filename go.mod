module gitlab.com/NebulousLabs/siamux

go 1.13

require (
	github.com/gorilla/websocket v1.5.0
	gitlab.com/NebulousLabs/encoding v0.0.0-20200604091946-456c3dc907fe
	gitlab.com/NebulousLabs/errors v0.0.0-20200929122200-06c536cf6975
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20211002182029-11da932010b6
	gitlab.com/NebulousLabs/log v0.0.0-20210609172545-77f6775350e2
	gitlab.com/NebulousLabs/persist v0.0.0-20200605115618-007e5e23d877
	gitlab.com/NebulousLabs/threadgroup v0.0.0-20200608151952-38921fbef213
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
	golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced
	golang.org/x/sys v0.0.0-20220808155132-1c4a2a72c664 // indirect
)
