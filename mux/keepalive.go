package mux

import "time"

var (
	// TimeoutNotificationBuffer is the time before the maxTimeout is reached
	// when the mux calls the registered timeout callback method.
	TimeoutNotificationBuffer = 5 * time.Minute // 5 minutes / 25% of the default maxTimeout
)

// updateDeadline handles extending the timeout of the mux's underlying
// connection whenever a frame was read or written.
func (m *Mux) updateDeadline() {
	// Compute the duration until the next deadline.
	duration := time.Second * time.Duration(m.settings.MaxTimeout)
	oldDeadline := m.timeoutDeadlineTime
	m.timeoutDeadlineTime = time.Now().Add(duration)

	// Only notify the callback thread if the timeout was moved to happen
	// earlier than before.
	if m.timeoutDeadlineTime.After(oldDeadline) {
		return // done
	}
	select {
	case m.timeoutUpdated <- struct{}{}:
	default:
	}
}

// threadedHandleMaxTimeoutCallback calls the timeout callback of the mux every
// time the mux is about to time out.
func (m *Mux) threadedHandleMaxTimeoutCallback() {
	var lastCallbackFireDeadline time.Time
	for {
		// Fetch the deadline and compute the notification time.
		m.staticMu.Lock()
		deadline := m.timeoutDeadlineTime
		callBackFireDeadline := deadline.Add(-TimeoutNotificationBuffer)
		m.staticMu.Unlock()

		// Update deadline.
		err := m.staticConn.SetDeadline(deadline)
		if err != nil {
			m.staticLog.Logger.Print("threadedHandleMaxTimeoutCallback: failed to set deadline", err)
		}

		// If we need to fire the callback, do that. Once fired we poll
		// every second for potentiall updates to the deadline which
		// would cause the callback to be called again.
		if time.Until(callBackFireDeadline) <= 0 && deadline != lastCallbackFireDeadline {
			lastCallbackFireDeadline = deadline

			// Call the timeout callback in a goroutine to prevent any blocking
			// operations in the callback from preventing this loop from executing
			// or shutting down.
			go m.staticTimeoutCallback(m)
			select {
			case <-time.After(time.Second):
				continue
			case <-m.timeoutUpdated:
				continue
			case <-m.staticCtx.Done():
				return
			}
		}

		// Otherwise sleep until we want want to fire the deadline. Then
		// we decide whether we should actually fire it. If not, we
		// sleep again.
		select {
		case <-time.After(time.Until(callBackFireDeadline)):
		case <-m.timeoutUpdated:
		case <-m.staticCtx.Done():
			return
		}
	}
}
